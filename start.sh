#!/bin/bash

set -eu

## Make sure permissions are good to go!
chown -R cloudron:cloudron /app/data
cd /app/data

exec /app/code/code-server/bin/code-server --bind-addr=0.0.0.0:8000 --user-data-dir=/app/data --config /app/data/config.yaml --extensions-dir=/app/code/code-server/lib/vscode/extensions 
